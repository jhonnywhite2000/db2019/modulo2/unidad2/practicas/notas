<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "nota".
 *
 * @property int $idn
 * @property int $id
 * @property string $fecha
 * @property string $hora
 * @property string $mensaje
 *
 * @property Usuario $id0
 */
class Nota extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
    */
    public static function tableName()
    {
        return 'nota';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['fecha', 'hora'], 'safe'],
            [['mensaje'], 'string', 'max' => 250],
            [['id'], 'exist', 'skipOnError' => true, 'targetClass' => Usuario::className(), 'targetAttribute' => ['id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idn' => 'Idn',
            'id' => 'ID',
            'fecha' => 'Fecha',
            'hora' => 'Hora',
            'mensaje' => 'Mensaje',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getId0()
    {
        return $this->hasOne(Usuario::className(), ['id' => 'id']);
    }
    
    public function getNotas(){
        return $this->find()->where([
            "fecha"=>$this->fecha
        ])->all();
    }
    
    public static function getFechas(){
        return self::find()->select("fecha")->distinct();
    }
    

    
    
}
