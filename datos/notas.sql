﻿SET NAMES 'utf8';

DROP DATABASE IF EXISTS notas;

CREATE DATABASE IF NOT EXISTS notas
CHARACTER SET utf8
COLLATE utf8_spanish_ci;

--
-- Set default database
--
USE notas;
CREATE OR REPLACE TABLE usuario(
  id int AUTO_INCREMENT PRIMARY KEY,
  username varchar(50) NOT NULL,
  email varchar(80) NOT NULL,
  password varchar(250) NOT NULL,
  authKey varchar(250) NOT NULL,
  accessToken varchar(250) NOT NULL,
  activate tinyint(1) NOT NULL DEFAULT 0
  
  )


CREATE OR REPLACE TABLE  nota(
  idn int AUTO_INCREMENT PRIMARY KEY,
  id int, 
  fecha date,
  hora datetime,
  mensaje varchar(250),
  FOREIGN KEY(id) REFERENCES usuario(id)
);

